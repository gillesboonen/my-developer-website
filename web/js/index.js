  function xhrGetAbout() {
    return $.ajax({
      type: 'POST',
      url: '../pages/about.php',
      success: function(data) {
        $('#about').html(data);
      }
    });
  }

  function xhrGetIntro() {
    return $.ajax({
      type: 'POST',
      url: '../pages/intro.php',
      success: function(data) {
        $('#intro').html(data);
      }
    });
  }

  function xhrGetSamples() {
    return $.ajax({
      type: 'POST',
      url: '../pages/samples.php',
      success: function(data) {
        $('#samples').html(data);
      }
    });
  }

  function xhrGetSkills() {
    return $.ajax({
      type: 'POST',
      url: '../pages/skills.php',
      success: function(data) {
        $('#skills').html(data);
      }
    });
  }

  function xhrGetTools() {
    return $.ajax({
      type: 'POST',
      url: '../pages/tools.php',
      success: function(data) {
        $('#tools').html(data);
      }
    });
  }


  
  function xhrCreateDB($db_name) {
    return $.ajax({
      type: 'POST',
      url: '../samples/create_db.php',
      data: {db_name : $db_name},
      success: function(data) {
        console.log("Create DB success");
      }
    });
  }

  function xhrShowEmployeeTable($db_name) {
    return $.ajax({
      type: 'POST',
      url: '../samples/show_employee_table.php',
      data: {db_name : $db_name},
      success: function(data) {
        $('#show_employee_table').html(data);
      }
    });
  }

  function xhrAddEmployee($db_name) {
    var frmData = $('#create_employee_form').serializeArray();
    frmData.push({name: "db_name", value: $db_name});
    return $.ajax({
      type: 'POST',
      url: '../samples/add_employee.php',
      data: frmData,
      success: function(data) {
        console.log("Add employee success");
      }
    });
  }

  function xhrEditEmployee($db_name, $id) {
    var frmData = $('#edit_employee_form').serializeArray();
    frmData.push({name: "db_name", value: $db_name});
    frmData.push({name: "employee_id", value: $id});
    return $.ajax({
      type: 'POST',
      url: '../samples/edit_employee.php',
      data: frmData,
      success: function(data) {
        console.log("Edit employee success");
      }
    });
  }

  function xhrDeleteEmployee($db_name, $employee_id) {
    return $.ajax({
      type: 'POST',
      url: '../samples/delete_employee.php',
      data: {db_name : $db_name, employee_id : $employee_id},
      success: function(data) {
        console.log("Delete employee success");
      }
    });
  }

  function xhrSubmitForm() {
    return $.ajax({
      type: 'POST',
      data: $('#contact_form').serialize(),
      url: '../contact/submit_form.php',
      success: function(data) {
        if (data == "Mail sent") {
          alert("Contact form sent succesfully, I will get back to you as soon as possible.");
          setTimeout(() => {
            $("#contact").fadeOut();
            
          }, 1000);
        } else {
          alert("There was an issue sending the contact form, please try again later.");          
          setTimeout(() => {
            $("#contact").fadeOut();        
          }, 1000);
        }
      }
    });
  }

$(document).ready(function() { 
  var activePage;

  xhrGetIntro().done(function(){});
  xhrGetAbout().done(function(){});
  xhrGetSkills().done(function(){});
  xhrGetSamples().done(function(){});
  xhrGetTools().done(function(){});

  $("#nav_home").on("click", function(){
    $(".nav_buttons").removeClass("active");
    var pageName = $(this).attr('name');      
    $('html, body').animate({ scrollTop: 0 }, 500);
    $(this).addClass("active");   
    activePage = pageName;  
    window.location = '#'+pageName; 
  });

  $( ".nav_buttons" ).each(function() {
    $(this).on("click", function(){
        var pageName = $(this).attr('name');
        if (pageName == "about") {
          var scrollTop = $("body").scrollTop() + $("#"+pageName).offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          activePage = pageName;  
          window.location = '#'+pageName;
        } else if (pageName == "skills") {
          var scrollTop = $("body").scrollTop() + $("#"+pageName).offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          activePage = pageName;  
          window.location = '#'+pageName;
        } else if (pageName == "samples") {
          var scrollTop = $("body").scrollTop() + $("#"+pageName).offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          activePage = pageName;  
          window.location = '#'+pageName;
        } else if (pageName == "tools") {
          var scrollTop = $("body").scrollTop() + $("#"+pageName).offset().top;      
          $('html, body').animate({
              scrollTop: scrollTop
          }, 500);  
          activePage = pageName;  
          window.location = '#'+pageName;
        }
    });
  });
  
  $(document.body).on('click',"#create_db_button",function (event) {
    event.preventDefault();
    console.log("clicked");
    $db_name = $("#create_db_name").val();
    xhrCreateDB($db_name).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});
    });
    $(".create_db_form").css("display", "none");
  });

  $(document.body).on('click',"#create_employee_button",function (event) {
    event.preventDefault();
    $db_name = $(this).attr("name");  
    xhrAddEmployee($db_name).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});});
  });

  $(document.body).on('click',".delete_employee",function (event) {
      event.preventDefault();
      $db_name = $(this).attr("name");  
      $id = $(this).attr("id");  
      console.log($id);
      xhrDeleteEmployee($db_name, $id).done(function() {
        xhrShowEmployeeTable($db_name).done(function() {});});
  });

  $(document.body).on('click',".edit_employee",function (event) {
    event.preventDefault();
    $(".td_save_edit_employee").css("display", "inline-block");
    $(".td_edit_employee").css("display", "none");
    $('input[name ="edit_first_name"]').prop("readonly", false);
    $('input[name ="edit_last_name"]').prop("readonly", false);
    $('input[name ="edit_employee_age"]').prop("readonly", false);
    $('input[name ="edit_employee_salary"]').prop("readonly", false);
    $('input[name ="edit_employee_role"]').prop("readonly", false);
    $('.edit_employee_input').removeClass("input_noneditable");
  });

  $(document.body).on('click',".save_edit_employee",function (event) {
    event.preventDefault();
    $db_name = $(this).attr("name");  
    $id = $(this).attr("id");  
    $(".td_save_edit_employee").css("display", "none");
    $(".td_edit_employee").css("display", "inline-block");
    xhrEditEmployee($db_name, $id).done(function() {
      xhrShowEmployeeTable($db_name).done(function() {});});
      $('.edit_employee_input').addClass("input_noneditable");
      $('input[name ="edit_first_name"]').prop("readonly", true);
      $('input[name ="edit_last_name"]').prop("readonly", true);
      $('input[name ="edit_employee_age"]').prop("readonly", true);
      $('input[name ="edit_employee_salary"]').prop("readonly", true);
      $('input[name ="edit_employee_role"]').prop("readonly", true);
  });
});

 // Javascript Modal functions

window.onload = function() {
  // Get the modal
var modal = document.getElementById("contact");
// Get the button that opens the modal
var btn = document.getElementById("contact_btn");
// Get the button that submits the form
var submit_btn = document.getElementById("send_button");
// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];
// When the user clicks on the button, open the modal
btn.onclick = function() {
  modal.style.display = "block";
  $('#send_button').prop("disabled", false);
}

submit_btn.onclick = function() {
  $('#send_button').prop("disabled", true);
  xhrSubmitForm().done(function() {});
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}

}