<?php
?>
<div id="about_container">
    <div class="page_content">
        <div id="about_image" class="left_page">
            <img id="picture_gilles" src="../img/picture_gilles.JPG" alt="Gilles in Valencia">
        </div>
        <div class="right_page">
            <div class="right_content">
                <p>A life before IT is hard to remember for Gilles. 
                    From feeding his inner Picasso using MS Paint on a Windows 95 machine, trying to install ‘free’ foreign video games (on a Windows 98), learning to re-install the computer (thanks to those video games), all the way to modern day; where learning new Linux distributions and various programming languages are part of the recurring curriculum. 
                    Gilles’ road of all things digital stretches a long way back, and seems to stretch ahead long ways as well. <br/><br/>
                    Spent a time in Ireland, shining as a Technical Support Analyst at HP; returned to Belgium to work for 3 years as a Technical Support Engineer consultant gaining experience with various different companies after which he moved to Spain for 3 years. 
                    While working for EA in sunny Madrid as a video game tester, he spent quite some leisure time talking to Dennis, a like-minded, IT-loving individual, and it was in those conversations he found the motivation to learn programming; and ultimately discovered his new calling, and a project (Work name: <a target="_blank" href="http://app.lemonchocolate.biz">LemonChocolate</a>*) to learn and deploy it with. 
                    It allowed him a new level of satisfaction and self-attainment previously unseen before.<br/><br/>
                    Today Gilles is back in Belgium, ready to kick off a career in software development, to learn from those with experience to surely one day become an expert developer, creating beautiful and intelligent solutions along the way.
                </p>
                <p class="p_extra"><br/><br/>*Tour of the application can be provided in recruitment process</p>
            </div>
        </div>
    </div>
</div>
