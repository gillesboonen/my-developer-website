<?php
?>
<div id="tools_container">
    <div class="page_content tools">
        <div class="left_page">
            <div class="page_header">
                    <p class="page_title">tools</p>
            </div>
        </div>
        <div class="right_page">
            <div class="right_content">
                <div class="tool_frame">
                    <p class="tool_title">Coding</p>
                    <div class="tool_unit">Google</div>
                    <div class="tool_unit">VSCodium</div>
                    <div class="tool_unit">Firefox</div>
                    <div class="tool_unit">PHPMyAdmin</div>
                    <div class="tool_unit">Terminal</div>
                    <div class="tool_unit">Docker / Compose</div>
                    <div class="tool_unit">Gitlab</div>
                </div>
                <div class="tool_frame">
                    <p class="tool_title">General Experience</p>
                    <div class="tool_unit">Windows (10, 7, XP, ..), MS Office, Active Directory</div>
                    <div class="tool_unit">Linux (Fedora, Ubuntu, PopOS, Arch, ..)</div>
                    <div class="tool_unit">Brave / Chrome / Opera / Vivaldi</div>
                    <div class="tool_unit">BMC Remedy, Jira, Devtrack</div>
                    <div class="tool_unit">Aventail, Cisco AnyConnect</div>
                    <div class="tool_unit">Android, Android Studio</div>
                    <div class="tool_unit">TeamViewer, Citrix</div>
                    <div class="tool_unit">Bitwarden</div>
                    <div class="tool_unit">Clubhouse</div>
                    <div class="tool_unit">Filezilla</div>
                    <div class="tool_unit">Gimp</div>
                    <div class="tool_unit">RSA</div>
                    <div class="tool_unit">iOS</div>
                </div>
            </div>
        </div>
    </div>
</div>
