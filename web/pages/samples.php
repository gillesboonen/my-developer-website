<?php
?>
<div id="samples_container">
    <div class="page_content">
        <div class="left_page">
            <div class="page_header">
                    <p class="page_title">sample</p>
                    <a target="_blank" href="https://gitlab.com/Meliorasa/my-developer-website/-/tree/master/web">
                        <img class="sample_icon" src="../img/gitlab.png" alt="GitLab">
                    </a>
            </div>
        </div>
        <div class="right_page">
            <div class="right_content">
                <p class="sample_title">Employee database</p>
                <p>Create (if not exists) and show an employee database table and add/modify employee details</p>
                <div class="sample_container">
                    <form class="create_db_form" name="create_db_form" onsubmit="return false" method="post">
                        <input type="text" name="db_name" id="create_db_name" value="Database name" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">
                        <input type="submit" id="create_db_button" class="db_button" value="Create">
                    </form>
                    <div id="show_employee_table"></div>
                </div>
            </div>
        </div>
    </div>
</div>
