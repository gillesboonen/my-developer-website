<?php
?>
<div id="skills_container">
    <div class="page_content">
        <div class="left_page">
            <div class="page_header">
                <p class="page_title">skills</p>
                <p class="skill_extra"><br/><b>+++</b> most experienced</p>
                <p class="skill_extra"><b>++</b> adequate experience</p>
                <p class="skill_extra"><b>+</b> introductory knowledge</p>
            </div>

        </div>
        <div class="right_page">
            <div class="right_content">
                <div class="skill_frame">
                    <p class="skill_title">Back-end</p>
                    <div class="skill_unit">PHP <b>+++</b></div>
                    <div class="skill_unit">Python (Flask) <b>++</b></div>
                </div>
                <div class="skill_frame">
                    <p class="skill_title">Front-end</p>
                    <div class="skill_unit">HTML <b>++</b></div>
                    <div class="skill_unit">CSS <b>++</b></div>
                    <div class="skill_unit">jQuery <b>++</b></div>
                </div>
                    <div class="skill_frame">
                    <p class="skill_title">DB</p>
                    <div class="skill_unit">MySQL <b>++</b></div>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
