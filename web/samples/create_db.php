
<?php

require_once(__DIR__.'/sample_1.php');

$db_name = $_POST['db_name'];
$sample = new Samples;

$db = $sample->CreateEmployeeDatabase($db_name);
$table = $sample->CreateEmployeeTable($db_name);

return $table;