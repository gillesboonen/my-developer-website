
<?php

require_once(__DIR__.'/sample_1.php');

$first_name = $_POST['first_name'];
$last_name = $_POST['last_name'];
$age = $_POST['employee_age'];
$role = $_POST['employee_role'];
$wage = $_POST['employee_salary'];
$db_name = $_POST['db_name'];

$sample = new Samples;

$add_employee = $sample->addEmployee($db_name, $first_name, $last_name, $age, $role, $wage);

return $add_employee;