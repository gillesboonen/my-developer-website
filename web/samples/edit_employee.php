
<?php

require_once(__DIR__.'/sample_1.php');

$first_name = $_POST['edit_first_name'];
$last_name = $_POST['edit_last_name'];
$age = $_POST['edit_employee_age'];
$role = $_POST['edit_employee_role'];
$salary = $_POST['edit_employee_salary'];
$db_name = $_POST['db_name'];
$employee_id = $_POST['employee_id'];

$sample = new Samples;

$edit_employee = $sample->editEmployee($db_name, $employee_id, $first_name, $last_name, $age, $role, $salary);

return $edit_employee;