<?php

require_once(__DIR__.'/sample_db.php');


class Samples {

    public function calculateVAT($netto, $percentage) { 
        $vat = $netto *$percentage/100;

        return $vat;
    }

    public function wash_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data, ENT_QUOTES);
        $data = preg_replace('!\s+!', ' ', $data);
        $data = preg_replace('/[^0-9a-zA-Z_]/', '', $data);
        if ($data == '') {
            $data = NULL;
        }; 
        return $data;
    }

    public function createEmployeeDatabase($db_name) { 
        $be = new dataBase;
        $db = $be->getDatabaseCredentials();		
        $conn = mysqli_connect($be->servername, $db['user'], $db['password']);
        mysqli_set_charset($conn, 'utf8');

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $db_name = $this->wash_input($db_name);

        $stmts["create_db"] = "CREATE DATABASE `".$db_name."`";
        // CHANGE
        if (!mysqli_stmt_prepare($stmt, $stmts["create_db"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            echo "Prep fail create db";
            echo $ar['dbc']['error'];
            return;
        } else { 
            // Execute statement
            if ($stmt->execute()) {
                $ar['dbc']['result'] = 1;
                return $db_name;
            } else {
                $error = $stmt->error;
                return "fail create db : " . $stmt->error;
            }
        }
    }

    public function createEmployeeTable($db_name) {
        $be = new dataBase;
        $db = $be->getDatabaseCredentials();		
        $db_name = $this->wash_input($db_name);
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db_name);
        mysqli_set_charset($conn, 'utf8');
        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $stmts["create_table"] = "CREATE TABLE Employees (
            employee_id int NOT NULL AUTO_INCREMENT,
            first_name varchar(255),
            last_name varchar(255),
            employee_age int,
            employee_salary int,
            employee_role varchar(255),
            PRIMARY KEY (employee_id)
        );";

        if (!mysqli_stmt_prepare($stmt, $stmts["create_table"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            echo "Prep fail create table";
            echo $ar['dbc']['error'];
            return;
        } else { 
            // Execute statement
            if ($stmt->execute()) {
                return $db_name;
            } else {
                $error = $stmt->error;
                return "fail create table : " . $stmt->error;
            }
        }
    }

    public function showEmployeeTable($db_name) {
        $be = new dataBase;
        $db = $be->getDatabaseCredentials();		
        $db_name = $this->wash_input($db_name);
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db_name);
        mysqli_set_charset($conn, 'utf8');

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];
        $employees = [];

        $stmts["show_table"] = "SELECT employee_id, first_name, last_name, employee_age, employee_salary, employee_role FROM Employees;";
        // CHANGE
        if (!mysqli_stmt_prepare($stmt, $stmts["show_table"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            echo "Prep fail show table";
            echo $ar['dbc']['error'];
            return;
        } else { 
            // Execute statement
            if ($stmt->execute()) {
                $result = $stmt->get_result();
                while ($row = $result->fetch_assoc()) {
                    $employees[] = $row;
                }
            } else {
                $error = $stmt->error;
                return "fail show table : " . $stmt->error;
            }
        }
        $employee_table = "<table class=\"employee_table\">
                                <thead>
                            <tr>
                                <th class=\"th_db_name\" colspan=\"7\">".$db_name."</th>
                            </tr>
                            <tr class=\"create_employee_form\">
                                <form id=\"create_employee_form\" name=\"create_employee_form\" onsubmit=\"return false\" method=\"post\">
                                    <td class=\"td_size_3\"><input type=\"text\" form=\"create_employee_form\" class=\"employee_input\" name=\"first_name\" id=\"\" value=\"First name\" onfocus=\"if(this.value==this.defaultValue)this.value='';\" onblur=\"if(this.value=='')this.value=this.defaultValue;\"></td>
                                    <td class=\"td_size_3\"><input type=\"text\" form=\"create_employee_form\" class=\"employee_input\" name=\"last_name\" id=\"\" value=\"Last name\" onfocus=\"if(this.value==this.defaultValue)this.value='';\" onblur=\"if(this.value=='')this.value=this.defaultValue;\"></td>
                                    <td class=\"td_size_2\"><input type=\"text\" form=\"create_employee_form\" class=\"employee_input_age\" name=\"employee_age\" id=\"\" value=\"Age\" onfocus=\"if(this.value==this.defaultValue)this.value='';\" onblur=\"if(this.value=='')this.value=this.defaultValue;\"></td>
                                    <td class=\"td_size_3\"><input type=\"text\" form=\"create_employee_form\" class=\"employee_input\" name=\"employee_role\" id=\"\" value=\"Role\" onfocus=\"if(this.value==this.defaultValue)this.value='';\" onblur=\"if(this.value=='')this.value=this.defaultValue;\"></td>
                                    <td class=\"td_size_2\"><input type=\"text\" form=\"create_employee_form\" class=\"employee_input\" name=\"employee_salary\" id=\"\" value=\"Salary\" onfocus=\"if(this.value==this.defaultValue)this.value='';\" onblur=\"if(this.value=='')this.value=this.defaultValue;\"></td>
                                    <td colspan=\"2\"><button name=".$db_name." id=\"create_employee_button\">Create</button></td>
                                </form>
                            </tr></thead><tbody>";
        foreach ($employees as $key) {
           $employee_table .= "<tr class=\"edit_employee_form\">
                                <form name=".$key['employee_id']." class=\"edit_employee_form\" id=\"edit_employee_form\" onsubmit=\"return false\" method=\"post\">
                                    <td class=\"td_size_1\"><input type =\"text\" form=\"edit_employee_form\" class=\"edit_employee_input input_noneditable\" name=\"employee_id\" value='".$key['employee_id']."' readonly></td>
                                    <td class=\"td_size_3\"><input type =\"text\" form=\"edit_employee_form\" class=\"edit_employee_input input_noneditable\" name=\"edit_first_name\" value='".$key['first_name']."' readonly></td>
                                    <td class=\"td_size_3\"><input type =\"text\" form=\"edit_employee_form\" class=\"edit_employee_input input_noneditable\" name=\"edit_last_name\" value='".$key['last_name']."' readonly></td>
                                    <td class=\"td_size_1\"><input type =\"text\" form=\"edit_employee_form\" class=\"edit_employee_input input_noneditable\" name=\"edit_employee_age\" value='".$key['employee_age']."' readonly></td>
                                    <td class=\"td_size_3\"><input type =\"text\" form=\"edit_employee_form\" class=\"edit_employee_input input_noneditable\" name=\"edit_employee_role\" value='".$key['employee_role']."' readonly></td>
                                    <td class=\"td_size_2\"><input type =\"text\" form=\"edit_employee_form\" class=\"edit_employee_input input_noneditable\" name=\"edit_employee_salary\" value='".$key['employee_salary']."' readonly></td>
                                    <td><button name=".$db_name." class=\"delete_employee db_button\" id=".$key['employee_id'].">Delete</button></td>
                                    <td class=\"td_edit_employee\"><button name=".$db_name." class=\"edit_employee db_button\" id=".$key['employee_id']." >Edit</button></td>
                                    <td class=\"td_save_edit_employee\"><button name=".$db_name." class=\"save_edit_employee db_button\" id=".$key['employee_id']." >Save</button></td>
                                </form>
                            </tr>";
        }
        $employee_table .= "</tbody></table>";

        return $employee_table;
    }

    public function addEmployee($db_name, $first_name, $last_name, $age, $role, $salary) {
        $be = new dataBase;
        $db = $be->getDatabaseCredentials();		
        $db_name = $this->wash_input($db_name);
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db_name);
        mysqli_set_charset($conn, 'utf8');

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $stmts["create_db"] = "INSERT INTO Employees (first_name, last_name, employee_age, employee_salary, employee_role) VALUES (?, ?, ?, ?, ?)";
        // CHANGE
        if (!mysqli_stmt_prepare($stmt, $stmts["create_db"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            echo "Prep fail new employee";
            echo $ar['dbc']['error'];
            return;
        } else { 
            $stmt->bind_param("ssiis", $this->wash_input($first_name), $this->wash_input($last_name), $this->wash_input($age), $this->wash_input($salary), $this->wash_input($role));
            // Execute statement
            if ($stmt->execute()) {
                $ar['dbc']['result'] = 1;
                return 1;
            } else {
                $error = $stmt->error;
                return "fail create employee : " . $stmt->error;
            }
        }
    }

    public function deleteEmployee($db_name, $employee_id) {
        $be = new dataBase;
        $db = $be->getDatabaseCredentials();		
        $db_name = $this->wash_input($db_name);
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db_name);
        mysqli_set_charset($conn, 'utf8');
        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $stmts["delete_employee"] = "DELETE FROM Employees WHERE employee_id = ?";
        // CHANGE
        if (!mysqli_stmt_prepare($stmt, $stmts["delete_employee"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            echo "Prep fail delete employee";
            echo $ar['dbc']['error'];
            return;
        } else {  
            $stmt->bind_param("i", $this->wash_input($employee_id));
             // Execute statement
            if ($stmt->execute()) {
                $ar['dbc']['result'] = 1;
                return 1;
            } else {
                $error = $stmt->error;
                return "fail delete employee : " . $stmt->error;
            }
        }
    }

    public function editEmployee ($db_name, $id, $first_name, $last_name, $age, $role, $salary) {
        $be = new dataBase;
        $db = $be->getDatabaseCredentials();		
        $db_name = $this->wash_input($db_name);
        $conn = mysqli_connect($be->servername, $db['user'], $db['password'], $db_name);
        mysqli_set_charset($conn, 'utf8');

        $stmt = mysqli_stmt_init($conn);
        $stmts = [];

        $stmts["edit_employee"] = "UPDATE Employees SET first_name=?, last_name=?, employee_age =?, employee_salary=?, employee_role=? WHERE employee_id=?";
        // CHANGE
        if (!mysqli_stmt_prepare($stmt, $stmts["edit_employee"])) {
            $ar['dbc']['result'] = 0;
            $ar['dbc']['error'] = $stmt->error;
            echo "Prep fail edit employee";
            echo $ar['dbc']['error'];
            return;
        } else { 
            $stmt->bind_param("ssiisi", $this->wash_input($first_name), $this->wash_input($last_name), $this->wash_input($age), $this->wash_input($salary), $this->wash_input($role), $this->wash_input($id));
            // Execute statement
            if ($stmt->execute()) {
                $ar['dbc']['result'] = 1;
                return 1;
            } else {
                $error = $stmt->error;
                return "fail edit employee : " . $stmt->error;
            }
        }
    }

}