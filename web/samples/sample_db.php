<?php

class dataBase {

	public $servername;

	function __construct() {
		$url = 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
		if (strpos($url,'gillesboonen') !== false) {
			$this->servername = 'localhost:3306'; // [i] production environment
		} else {
			$this->servername = 'database:3306'; // [i] local development environment
		}
	  }
	public $database = array(
		'sample' => array(
			'name' => 'sample_db',
			'user' => 'test_user',
			'password' => 'test_password'
		)
	);
	

	public function getDatabaseCredentials() {
			$this->database['sample']['name'] = $_SESSION['db_name'];
			return $this->database['sample'];
	}


}
